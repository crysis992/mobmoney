package org.avalon.mobmoney;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.NumberConversions;

import net.milkbowl.vault.economy.EconomyResponse;

public class MobKillListener implements Listener {

	private  MobMoney plugin;
	private Random rand;

	public MobKillListener(MobMoney instance) {
		plugin = instance;
		rand = new Random();
	}

	@EventHandler
	public void onSpawn(CreatureSpawnEvent e) {
		if (e.getSpawnReason() == SpawnReason.SPAWNER_EGG && !plugin.getConfig().getBoolean("main.AllowSpawnEggs")) {
			e.getEntity().setMetadata("mobmoney", new FixedMetadataValue(plugin, "spawner"));
			return;
		}

		else if (e.getSpawnReason() == SpawnReason.SPAWNER && !plugin.getConfig().getBoolean("main.AllowMonsterSpawner")) {
			e.getEntity().setMetadata("mobmoney", new FixedMetadataValue(plugin, "spawner"));
			return;
		}
	}

	@EventHandler
	public void onMobKill(EntityDeathEvent e) {
		if (!(e.getEntity().getKiller() instanceof Player)) {
			return;
		}
		
		Player p = (Player) e.getEntity().getKiller();
		
		if (e.getEntity().hasMetadata("mobmoney")) { return; }
		if (!p.hasPermission("mobmoney.receive")) { return; }
		if (!plugin.getConfig().getBoolean("main.AllowCreativeMode") && e.getEntity().getKiller().getGameMode() == GameMode.CREATIVE) { return; }
		if (plugin.getConfig().getStringList("World_Blacklist").contains(e.getEntity().getWorld().getName())) { return; }
		
		Entity ent = e.getEntity();
		World w = e.getEntity().getWorld();
		
		if (rand.nextInt(100) <= plugin.getConfig().getInt("main.ChanceToDropMoney")) {
			if (plugin.max.containsKey(e.getEntityType().name())) {
				if (Item(e.getEntityType().name()) != null) {
					if (!plugin.getConfig().getBoolean("main.double_event")) {
						w.dropItem(ent.getLocation(), Item(e.getEntityType().name()));
						return;
					} else {
						w.dropItem(ent.getLocation(), Item(e.getEntityType().name()));
						w.dropItem(ent.getLocation(), Item(e.getEntityType().name()));
						return;
					}
				}
			}
		}
	}

	private ItemStack Item(String mobname) {
		ItemStack i = plugin.i;
		ItemMeta meta = i.getItemMeta();
		double max = plugin.max.get(mobname);
		double min = plugin.min.get(mobname);
		if (max == 0.0 && min == 0.0) {
			return null;
		} else {
			double result = min + (max - min) * rand.nextDouble();
			if (result == 0.0) {
				return null;
			}
			meta.setDisplayName("MobMoney");
			List<String> lore = new ArrayList<String>();
			lore.add(RoundTo2Decimals(result));
			meta.setLore(lore);
			i.setItemMeta(meta);
			return i;
		}
	}

	@EventHandler(ignoreCancelled = true)
	public void onMoneyPickup(PlayerPickupItemEvent e) {		
		if (e.getItem().getItemStack().hasItemMeta()) {
			ItemStack i = e.getItem().getItemStack();
			Player p = e.getPlayer();
			if (i.getItemMeta().getDisplayName() != null && i.getItemMeta().getDisplayName().equals("MobMoney")) {
				float money = getAmount(i);
				e.setCancelled(true);
				e.getItem().remove();
				if (money == 0.0) {
					return;
				}
				if (add(p, money)) {
					if (plugin.getConfig().getBoolean("main.sendpickupmessage")) {
						String rewardMessage = plugin.getConfig().getString("language.pickup").replace("%money%",RoundTo2Decimals(money) + " " + plugin.econ.currencyNameSingular());
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', rewardMessage));
					}
					p.playSound(p.getLocation(), plugin.getConfig().getString("main.pickupsound"), plugin.getConfig().getInt("main.pickupsound-volume"), plugin.getConfig().getInt("main.pickupsound-pitch"));
				}
			}
		}
	}

	private boolean add(Player player, float money) {
		EconomyResponse r = plugin.econ.depositPlayer(player, money);
		if (r.transactionSuccess()) {
			return true;
		} else {
			player.sendMessage(ChatColor.RED + "Error while adding money to your account");
			return false;
		}
	}
	
	
	private String RoundTo2Decimals(double d) {
		return Double.toString(Math.round(d * 100) / 100);
	}

	protected float getAmount(ItemStack itemStack) {
		ItemMeta meta = itemStack.getItemMeta();
		if (meta == null) {
			return 0.0f;
		}
		List<String> lore = meta.getLore();
		if (lore.size() == 0) {
			return 0.0f;
		}
		float value = NumberConversions.toFloat(lore.get(0));
		return value * itemStack.getAmount();

	}
}