package org.avalon.mobmoney;

import java.io.File;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import net.milkbowl.vault.economy.Economy;

public class MobMoney extends JavaPlugin {
	
	private static MobMoney instance;
	
	public Economy econ = null;
	public HashMap<String, Double> max = new HashMap<String, Double>();
	public HashMap<String, Double> min = new HashMap<String, Double>();
	
	public ItemStack i;
	
	@Override
	public void onLoad() {
		MobMoney.instance = this;
	}
	
	@Override
	public void onEnable() {
		if (!setupEconomy()) {
			getLogger().warning(String.format("[%s] - Could not load Plugin, no Vault dependency found.", new Object[] { getDescription().getName() }));
			getServer().getPluginManager().disablePlugin(this);
			return;	
	} else {
		getLogger().info("[MobGroups] Hooked " + econ.getName());
	}
		File configlist = new File(this.getDataFolder().getAbsolutePath() + File.separator + "config.yml");
		if (!this.getDataFolder().exists()) {
			this.getDataFolder().mkdir();
		}
		if (!configlist.exists()) {
			FileUtils.setupMainConfig();	
		}
		FileUtils.updateConfig();
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new MobKillListener(this), this);
		setupreward();
		setupvar();
	}
	
	  private boolean setupEconomy() {
		    if (getServer().getPluginManager().getPlugin("Vault") == null) {
		      return false;
		    }
		    RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		    if (rsp == null) {
		      return false;
		    }
		    econ = (Economy)rsp.getProvider();
		    return econ != null;
		  }
	  
	  
	  private void setupreward() {
		  ConfigurationSection conf = this.getConfig().getConfigurationSection("rewards");
		  
		  for (String key : conf.getKeys(false)) {
			  
			  ConfigurationSection v = conf.getConfigurationSection(key);
			  String k = key.toUpperCase();
			  
			  EntityType type = EntityType.valueOf(k);
			  if (type == null) {
				  Bukkit.getLogger().warning(key + " is not a valid mob name.");
				  continue;
			  }
			  min.put(type.name(), v.getDouble("minimum"));
			  max.put(type.name(), v.getDouble("maximum")); 
		  }
	  }
	  
	  private void setupvar() {
			
			String reward = getConfig().getString("main.droppedItem").replace(':', ' ');
			String[] r = reward.split(" ");
			
			Material mat = Material.getMaterial(r[0].toUpperCase());
			if (mat == null) {
				 Bukkit.getLogger().warning(r[0] + " is not a valid material name!");
			}
			int SubID;
			if (r.length == 1) {
				SubID = 0;
			} else {
				SubID = Integer.parseInt(r[1]);
			}
			i = new ItemStack(mat, 1, (short) SubID);
	  }
	  
	   public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		   if (!cmd.getName().equals("mobmoney")) { return true; }

		   if (args.length == 1 && args[0].equalsIgnoreCase("reload")) {
			   if (sender.hasPermission("mobmoney.reload")) {
				   this.reloadConfig();
				   sender.sendMessage(ChatColor.GREEN + "The Configuration has been reloaded!");
				   return true;
			   } else {
				   sender.sendMessage(ChatColor.RED + "You lack the proper permissions to use this command.");
				   return true;
			   }
		   } else {
		    	 sender.sendMessage(ChatColor.GREEN + "=== MobMoney ===");
		    	 sender.sendMessage(ChatColor.GOLD + "/mobmoney reload - Reload the configuration.");
		    	 sender.sendMessage(ChatColor.GREEN + "=== MobMoney ===");
		    	 return true;
		   }
	   }
	   
	public static MobMoney getInstance() {
		return MobMoney.instance;
	}
	   
	@Override
	public void reloadConfig() {
		   this.max.clear();
		   this.min.clear();
		   super.reloadConfig();
		   setupreward();
		   setupvar();
	}
}